# Script that Counts IPv6 Relays in the Consensus

See `ipv6.py` for usage information and [tpo/metrics/trac#40002](https://gitlab.torproject.org/tpo/metrics/trac/-/issues/40002) for context.

