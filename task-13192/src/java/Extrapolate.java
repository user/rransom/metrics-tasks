import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileWriter;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TimeZone;
import java.util.TreeMap;
import java.util.TreeSet;

import org.torproject.descriptor.Descriptor;
import org.torproject.descriptor.DescriptorFile;
import org.torproject.descriptor.DescriptorReader;
import org.torproject.descriptor.DescriptorSourceFactory;
import org.torproject.descriptor.ExtraInfoDescriptor;
import org.torproject.descriptor.NetworkStatusEntry;
import org.torproject.descriptor.RelayNetworkStatusConsensus;

public class Extrapolate {

  private static File archiveExtraInfosDirectory =
      new File("in/collector/archive/relay-descriptors/extra-infos/");

  private static File recentExtraInfosDirectory =
      new File("in/collector/recent/relay-descriptors/extra-infos/");

  private static File archiveConsensuses =
      new File("in/collector/archive/relay-descriptors/consensuses/");

  private static File recentConsensuses =
      new File("in/collector/recent/relay-descriptors/consensuses/");

  private static File hidservStatsCsvFile =
      new File("out/csv/hidserv-stats.csv");

  public static void main(String[] args) throws Exception {
    System.out.println("Extracting hidserv-* lines from extra-info "
        + "descriptors...");
    SortedMap<String, SortedSet<HidServStats>> hidServStats =
        extractHidServStats();
    System.out.println("Extracting fractions from consensuses...");
    SortedMap<String, SortedSet<ConsensusFraction>> consensusFractions =
        extractConsensusFractions(hidServStats.keySet());
    System.out.println("Extrapolating statistics...");
    extrapolateHidServStats(hidServStats, consensusFractions);
    System.out.println(new Date() + " Terminating.");
  }

  private static final DateFormat DATE_TIME_FORMAT;

  static {
    DATE_TIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    DATE_TIME_FORMAT.setLenient(false);
    DATE_TIME_FORMAT.setTimeZone(TimeZone.getTimeZone("UTC"));
  }

  private static class HidServStats implements Comparable<HidServStats> {

    /* Hidden-service statistics end timestamp in milliseconds. */
    private long statsEndMillis;

    /* Statistics interval length in seconds. */
    private long statsIntervalSeconds;

    /* Number of relayed cells reported by the relay and adjusted by
     * rounding to the nearest right side of a bin and subtracting half of
     * the bin size. */
    private long rendRelayedCells;

    /* Number of .onions reported by the relay and adjusted by rounding to
     * the nearest right side of a bin and subtracting half of the bin
     * size. */
    private long dirOnionsSeen;

    private HidServStats(long statsEndMillis, long statsIntervalSeconds,
        long rendRelayedCells, long dirOnionsSeen) {
      this.statsEndMillis = statsEndMillis;
      this.statsIntervalSeconds = statsIntervalSeconds;
      this.rendRelayedCells = rendRelayedCells;
      this.dirOnionsSeen = dirOnionsSeen;
    }

    @Override
    public boolean equals(Object otherObject) {
      if (!(otherObject instanceof HidServStats)) {
        return false;
      }
      HidServStats other = (HidServStats) otherObject;
      return this.statsEndMillis == other.statsEndMillis &&
          this.statsIntervalSeconds == other.statsIntervalSeconds &&
          this.rendRelayedCells == other.rendRelayedCells &&
          this.dirOnionsSeen == other.dirOnionsSeen;
    }

    @Override
    public int compareTo(HidServStats other) {
      return this.statsEndMillis < other.statsEndMillis ? -1 :
          this.statsEndMillis > other.statsEndMillis ? 1 : 0;
    }
  }

  /* Extract fingerprint and hidserv-* lines from extra-info descriptors
   * located in in/{archive,recent}/relay-descriptors/extra-infos/. */
  private static SortedMap<String, SortedSet<HidServStats>>
      extractHidServStats() {
    SortedMap<String, SortedSet<HidServStats>> extractedHidServStats =
        new TreeMap<String, SortedSet<HidServStats>>();
    DescriptorReader descriptorReader =
        DescriptorSourceFactory.createDescriptorReader();
    descriptorReader.addDirectory(archiveExtraInfosDirectory);
    descriptorReader.addDirectory(recentExtraInfosDirectory);
    Iterator<DescriptorFile> descriptorFiles =
        descriptorReader.readDescriptors();
    while (descriptorFiles.hasNext()) {
      DescriptorFile descriptorFile = descriptorFiles.next();
      for (Descriptor descriptor : descriptorFile.getDescriptors()) {
        if (!(descriptor instanceof ExtraInfoDescriptor)) {
          continue;
        }
        String fingerprint =
            ((ExtraInfoDescriptor) descriptor).getFingerprint();
        Scanner scanner = new Scanner(new ByteArrayInputStream(
            descriptor.getRawDescriptorBytes()));
        Long statsEndMillis = null, statsIntervalSeconds = null,
            rendRelayedCells = null, dirOnionsSeen = null;
        try {
          while (scanner.hasNext()) {
            String line = scanner.nextLine();
            if (line.startsWith("hidserv-")) {
              String[] parts = line.split(" ");
              if (parts[0].equals("hidserv-stats-end")) {
                if (parts.length != 5 || !parts[3].startsWith("(") ||
                    !parts[4].equals("s)")) {
                  /* Will warn below, because statsEndMillis and
                   * statsIntervalSeconds are still null. */
                  continue;
                }
                statsEndMillis = DATE_TIME_FORMAT.parse(
                    parts[1] + " " + parts[2]).getTime();
                statsIntervalSeconds =
                    Long.parseLong(parts[3].substring(1));
              } else if (parts[0].equals("hidserv-rend-relayed-cells")) {
                if (parts.length != 5 ||
                    !parts[4].startsWith("bin_size=")) {
                  /* Will warn below, because rendRelayedCells is still
                   * null. */
                  continue;
                }
                rendRelayedCells = removeNoise(Long.parseLong(parts[1]),
                    Long.parseLong(parts[4].substring(9)));
              } else if (parts[0].equals("hidserv-dir-onions-seen")) {
                if (parts.length != 5 ||
                    !parts[4].startsWith("bin_size=")) {
                  /* Will warn below, because dirOnionsSeen is still
                   * null. */
                  continue;
                }
                dirOnionsSeen = removeNoise(Long.parseLong(parts[1]),
                    Long.parseLong(parts[4].substring(9)));
              }
            }
          }
        } catch (ParseException e) {
          e.printStackTrace();
          continue;
        } catch (NumberFormatException e) {
          e.printStackTrace();
          continue;
        }
        if (statsEndMillis == null && statsIntervalSeconds == null &&
            rendRelayedCells == null && dirOnionsSeen == null) {
          continue;
        } else if (statsEndMillis != null && statsIntervalSeconds != null
            && rendRelayedCells != null && dirOnionsSeen != null) {
          if (!extractedHidServStats.containsKey(fingerprint)) {
            extractedHidServStats.put(fingerprint,
                new TreeSet<HidServStats>());
          }
          extractedHidServStats.get(fingerprint).add(new HidServStats(
              statsEndMillis, statsIntervalSeconds, rendRelayedCells,
              dirOnionsSeen));
        } else {
          System.err.println("Relay " + fingerprint + " published "
              + "incomplete hidserv-stats.  Ignoring.");
        }
      }
    }
    return extractedHidServStats;
  }

  private static long removeNoise(long reportedNumber, long binSize) {
    long roundedToNearestRightSideOfTheBin =
        ((reportedNumber + binSize / 2) / binSize) * binSize;
    long subtractedHalfOfBinSize =
        roundedToNearestRightSideOfTheBin - binSize / 2;
    return subtractedHalfOfBinSize;
  }

  private static class ConsensusFraction
      implements Comparable<ConsensusFraction> {

    /* Valid-after timestamp of the consensus in milliseconds. */
    private long validAfterMillis;

    /* Fresh-until timestamp of the consensus in milliseconds. */
    private long freshUntilMillis;

    /* Probability for being selected by clients as rendezvous point. */
    private double probabilityRendezvousPoint;

    /* Probability for being selected as directory.  This is the fraction
     * of descriptors identifiers that this relay has been responsible
     * for, divided by 3. */
    private double fractionResponsibleDescriptors;

    private ConsensusFraction(long validAfterMillis,
        long freshUntilMillis,
        double probabilityRendezvousPoint,
        double fractionResponsibleDescriptors) {
      this.validAfterMillis = validAfterMillis;
      this.freshUntilMillis = freshUntilMillis;
      this.probabilityRendezvousPoint = probabilityRendezvousPoint;
      this.fractionResponsibleDescriptors =
          fractionResponsibleDescriptors;
    }

    @Override
    public boolean equals(Object otherObject) {
      if (!(otherObject instanceof ConsensusFraction)) {
        return false;
      }
      ConsensusFraction other = (ConsensusFraction) otherObject;
      return this.validAfterMillis == other.validAfterMillis &&
          this.freshUntilMillis == other.freshUntilMillis &&
          this.fractionResponsibleDescriptors ==
          other.fractionResponsibleDescriptors &&
          this.probabilityRendezvousPoint ==
          other.probabilityRendezvousPoint;
    }

    @Override
    public int compareTo(ConsensusFraction other) {
      return this.validAfterMillis < other.validAfterMillis ? -1 :
          this.validAfterMillis > other.validAfterMillis ? 1 : 0;
    }
  }

  /* Extract fractions that relays were responsible for from consensuses
   * located in in/{archive,recent}/relay-descriptors/consensuses/. */
  private static SortedMap<String, SortedSet<ConsensusFraction>>
      extractConsensusFractions(Collection<String> fingerprints) {
    SortedMap<String, SortedSet<ConsensusFraction>>
        extractedConsensusFractions =
        new TreeMap<String, SortedSet<ConsensusFraction>>();
    DescriptorReader descriptorReader =
        DescriptorSourceFactory.createDescriptorReader();
    descriptorReader.addDirectory(archiveConsensuses);
    descriptorReader.addDirectory(recentConsensuses);
    Iterator<DescriptorFile> descriptorFiles =
        descriptorReader.readDescriptors();
    while (descriptorFiles.hasNext()) {
      DescriptorFile descriptorFile = descriptorFiles.next();
      for (Descriptor descriptor : descriptorFile.getDescriptors()) {
        if (!(descriptor instanceof RelayNetworkStatusConsensus)) {
          continue;
        }
        RelayNetworkStatusConsensus consensus =
            (RelayNetworkStatusConsensus) descriptor;
        SortedSet<String> weightKeys = new TreeSet<String>(Arrays.asList(
            "Wmg,Wmm,Wme,Wmd".split(",")));
        weightKeys.removeAll(consensus.getBandwidthWeights().keySet());
        if (!weightKeys.isEmpty()) {
          System.err.println("Consensus with valid-after time "
              + DATE_TIME_FORMAT.format(consensus.getValidAfterMillis())
              + " doesn't contain expected Wmx weights.  Skipping.");
          continue;
        }
        double wmg = ((double) consensus.getBandwidthWeights().get("Wmg"))
            / 10000.0;
        double wmm = ((double) consensus.getBandwidthWeights().get("Wmm"))
            / 10000.0;
        double wme = ((double) consensus.getBandwidthWeights().get("Wme"))
            / 10000.0;
        double wmd = ((double) consensus.getBandwidthWeights().get("Wmd"))
            / 10000.0;
        SortedSet<String> hsDirs = new TreeSet<String>(
            Collections.reverseOrder());
        double totalWeightsRendezvousPoint = 0.0;
        SortedMap<String, Double> weightsRendezvousPoint =
            new TreeMap<String, Double>();
        for (Map.Entry<String, NetworkStatusEntry> e :
            consensus.getStatusEntries().entrySet()) {
          String fingerprint = e.getKey();
          NetworkStatusEntry statusEntry = e.getValue();
          SortedSet<String> flags = statusEntry.getFlags();
          if (flags.contains("HSDir")) {
            hsDirs.add(statusEntry.getFingerprint());
          }
          double weightRendezvousPoint = 0.0;
          if (flags.contains("Fast")) {
            weightRendezvousPoint = (double) statusEntry.getBandwidth();
            if (flags.contains("Guard") && flags.contains("Exit")) {
              weightRendezvousPoint *= wmd;
            } else if (flags.contains("Guard")) {
              weightRendezvousPoint *= wmg;
            } else if (flags.contains("Exit")) {
              weightRendezvousPoint *= wme;
            } else {
              weightRendezvousPoint *= wmm;
            }
          }
          weightsRendezvousPoint.put(fingerprint, weightRendezvousPoint);
          totalWeightsRendezvousPoint += weightRendezvousPoint;
        }
        /* Add all HSDir fingerprints with leading "0" and "1" to
         * simplify the logic to traverse the ring start. */
        SortedSet<String> hsDirsCopy = new TreeSet<String>(hsDirs);
        hsDirs.clear();
        for (String fingerprint : hsDirsCopy) {
          hsDirs.add("0" + fingerprint);
          hsDirs.add("1" + fingerprint);
        }
        final double RING_SIZE = new BigInteger(
            "10000000000000000000000000000000000000000",
            16).doubleValue();
        for (String fingerprint : fingerprints) {
          double probabilityRendezvousPoint = 0.0,
              fractionDescriptors = 0.0;
          NetworkStatusEntry statusEntry =
              consensus.getStatusEntry(fingerprint);
          if (statusEntry != null) {
            if (hsDirs.contains("1" + fingerprint)) {
              String startResponsible = fingerprint;
              int positionsToGo = 3;
              for (String hsDirFingerprint :
                  hsDirs.tailSet("1" + fingerprint)) {
                startResponsible = hsDirFingerprint;
                if (positionsToGo-- <= 0) {
                  break;
                }
              }
              fractionDescriptors =
                  new BigInteger("1" + fingerprint, 16).subtract(
                  new BigInteger(startResponsible, 16)).doubleValue()
                  / RING_SIZE;
              fractionDescriptors /= 3.0;
            }
            probabilityRendezvousPoint =
                weightsRendezvousPoint.get(fingerprint)
                / totalWeightsRendezvousPoint;
          }
          if (!extractedConsensusFractions.containsKey(fingerprint)) {
            extractedConsensusFractions.put(fingerprint,
                new TreeSet<ConsensusFraction>());
          }
          extractedConsensusFractions.get(fingerprint).add(
              new ConsensusFraction(consensus.getValidAfterMillis(),
              consensus.getFreshUntilMillis(), probabilityRendezvousPoint,
              fractionDescriptors));
        }
      }
    }
    return extractedConsensusFractions;
  }

  private static void extrapolateHidServStats(
      SortedMap<String, SortedSet<HidServStats>> hidServStats,
      SortedMap<String, SortedSet<ConsensusFraction>>
      consensusFractions) throws Exception {
    hidservStatsCsvFile.getParentFile().mkdirs();
    BufferedWriter bw = new BufferedWriter(
        new FileWriter(hidservStatsCsvFile));
    bw.write("fingerprint,stats_start,stats_end,"
        + "hidserv_rend_relayed_cells,hidserv_dir_onions_seen,"
        + "frac_rend_relayed_cells,frac_dir_onions_seen\n");
    for (Map.Entry<String, SortedSet<HidServStats>> e :
      hidServStats.entrySet()) {
      String fingerprint = e.getKey();
      if (!consensusFractions.containsKey(fingerprint)) {
        System.err.println("We have hidserv-stats but no consensus "
            + "fractions for " + fingerprint + ".  Skipping.");
        continue;
      }
      for (HidServStats stats : e.getValue()) {
        long statsStartMillis = stats.statsEndMillis
            - stats.statsIntervalSeconds * 1000L;
        double sumProbabilityRendezvousPoint = 0.0,
            sumResponsibleDescriptors = 0.0;
        int statusEntries = 0;
        for (ConsensusFraction frac :
            consensusFractions.get(fingerprint)) {
          if (statsStartMillis <= frac.validAfterMillis &&
              frac.validAfterMillis < stats.statsEndMillis) {
            sumProbabilityRendezvousPoint +=
                frac.probabilityRendezvousPoint;
            sumResponsibleDescriptors +=
                frac.fractionResponsibleDescriptors;
            statusEntries++;
          }
        }
        double fracCells = sumProbabilityRendezvousPoint / statusEntries,
            fracDescs = sumResponsibleDescriptors / statusEntries;
        bw.write(String.format("%s,%s,%s,%d,%d,%.8f,%.8f%n", fingerprint,
            DATE_TIME_FORMAT.format(statsStartMillis),
            DATE_TIME_FORMAT.format(stats.statsEndMillis),
            stats.rendRelayedCells, stats.dirOnionsSeen, fracCells,
            fracDescs));
      }
    }
    bw.close();
  }
}

